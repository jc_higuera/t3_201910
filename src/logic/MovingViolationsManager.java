package logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.GregorianCalendar;

import com.opencsv.CSVReader;
import com.sun.org.apache.xalan.internal.xsltc.compiler.Template;
import com.sun.xml.internal.messaging.saaj.packaging.mime.util.QDecoderStream;

import model.data_structures.LinkedQueue;
import model.data_structures.Stack;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;

public class MovingViolationsManager 
{

	private LinkedQueue<VOMovingViolations> linkedQueue = new LinkedQueue<>();
	private Stack<VOMovingViolations> stack = new Stack<>();

	public void loadMovingViolations(String movingViolationsFile){
		try {
			CSVReader reader = new CSVReader(new FileReader(movingViolationsFile));
			String [] nextLine;
			nextLine = reader.readNext();
			while ((nextLine = reader.readNext()) != null) {
				// nextLine[] is an array of values from the line
				linkedQueue.enqueue(new VOMovingViolations(nextLine[0], nextLine[2], nextLine[3], nextLine[4], nextLine[5], nextLine[6], nextLine[7], nextLine[8], nextLine[9], nextLine[10], nextLine[12], nextLine[13], nextLine[14], nextLine[15], nextLine[16]));
				stack.push(new VOMovingViolations(nextLine[0], nextLine[2], nextLine[3], nextLine[4], nextLine[5], nextLine[6], nextLine[7], nextLine[8], nextLine[9], nextLine[10], nextLine[12], nextLine[13], nextLine[14], nextLine[15], nextLine[16]));
			}
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(linkedQueue.size());
		System.out.println(stack.size());

	}

	public LinkedQueue<VODaylyStatistic> getDalyStatistics() throws IOException{
		LinkedQueue<VODaylyStatistic> dalyStatiscs = new LinkedQueue<>();
		LinkedQueue<VOMovingViolations> temp = new LinkedQueue<>() ;
		int numInfracciones = 0;
		int numAcidentes= 0;
		int totalFineAmt = 0;
		VOMovingViolations current = null;
		if(linkedQueue.isEmpty()){
			throw new IOException("no se ha cargado el archivo");
		}
		else{
			while(linkedQueue.iterator().hasNext()){
				current = linkedQueue.dequeue();
				if(current.getDate().equals(linkedQueue.iterator().next().getDate())){
					if(current.getAccident().equals("Yes")){
						numAcidentes ++;
					}
					numInfracciones ++;
					totalFineAmt = totalFineAmt+ current.getFine();
				}
				else{
					dalyStatiscs.enqueue(new VODaylyStatistic(current.getDate(), totalFineAmt, numAcidentes, numInfracciones));
					numInfracciones = 0;
					numAcidentes= 0;
					totalFineAmt = 0;
				}
			}
			if(numInfracciones!= 0){
				dalyStatiscs.enqueue(new VODaylyStatistic(current.getDate(), totalFineAmt, numAcidentes, numInfracciones));
			}
			temp.enqueue(current);
		}
		linkedQueue.copy(temp);

		return dalyStatiscs ;
	}

	public Stack<VOMovingViolations> nLastAccidents(int n)
	{
		Stack<VOMovingViolations> ans = new Stack<>();
		for (int i = 0; i < n && n<= stack.size(); i++) 
		{
			ans.push(stack.pop());
		}
		for (VOMovingViolations voMovingViolations : ans) {
			stack.push(voMovingViolations);
		}
		return ans;
	}

}
