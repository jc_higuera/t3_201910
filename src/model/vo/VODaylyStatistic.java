package model.vo;
import java.util.GregorianCalendar;

public class VODaylyStatistic {

	public GregorianCalendar getDay() {
		return day;
	}

	public void setDay(GregorianCalendar day) {
		this.day = day;
	}

	public int getTotalFineAmt() {
		return totalFineAmt;
	}

	public void setTotalFineAmt(int totalFineAmt) {
		this.totalFineAmt = totalFineAmt;
	}

	public int getNumAccidentes() {
		return numAccidentes;
	}

	public void setNumAccidentes(int numAccidentes) {
		this.numAccidentes = numAccidentes;
	}

	public int getNumInfracciones() {
		return numInfracciones;
	}

	public void setNumInfracciones(int numInfracciones) {
		this.numInfracciones = numInfracciones;
	}

	GregorianCalendar day;
	int totalFineAmt;
	int numAccidentes;
	int numInfracciones;

	 public VODaylyStatistic(GregorianCalendar pDay, int pTotal, int numA, int numI){ 

		day = pDay;
		totalFineAmt = pTotal;
		numAccidentes = numA;
		numInfracciones = numI;

	}
	 

}



