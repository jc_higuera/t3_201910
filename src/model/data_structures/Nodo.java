package model.data_structures;

public class Nodo<T> {
	private T data;
	private Nodo<T> siguiente;
	private Nodo<T> anterior;


	public Nodo( T pData, Nodo pNext, Nodo pPrev){
		data = pData;
		siguiente = pNext;
		anterior = pPrev;
	}


	/**
	 * Links the next Node. 
	 * @param next
	 */
	public void cambiarSiguiente(Nodo<T> pNext) {
		this.siguiente = pNext;
	}
	/**
	 * Get the Next Node.
	 * @return The next Node.
	 */
	public Nodo<T> darSiguiente() {
		return siguiente;
	}
	/**
	 * Links the next Node. 
	 * @param next
	 */
	public void cambiarAnterior(Nodo<T> pPrev) {
		this.anterior = pPrev;
	}
	/**
	 * Get the Next Node.
	 * @return The next Node.
	 */
	public Nodo<T> darAnterior() {
		return anterior;
	}
	/**
	 * Return the current Node.
	 * @return The current Node.
	 */
	public T darData() {
		return data;
	}
	public void cambiarData(T pData){
		data = pData;
	}


}
