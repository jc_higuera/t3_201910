package model.data_structures;
//Tomado de Algorithms 4ta edición

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedQueue<T> implements IQueue<T> {

	private int cont;
	private Nodo<T> first; 
	private Nodo<T> last;

	public LinkedQueue() {
		// TODO Auto-generated constructor stub
		first = null;
		last = null;
		cont = 0;
	}


	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return first == null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return cont;
	}

	@Override
	public void enqueue(T data) {
		// TODO Auto-generated method stub
		Nodo oldlast = last;
		last = new Nodo(null,null,null);
		last.cambiarData(data);
		last.cambiarSiguiente(null);
		if (isEmpty()) first = last;
		else           oldlast.cambiarSiguiente( last);
		cont++;


	}

	@Override
	public T dequeue() {
		if (isEmpty()) throw new NoSuchElementException("Queue underflow");
		T item = first.darData();
		first = first.darSiguiente();
		cont--;
		if (isEmpty()) last = null;   // to avoid loitering
		return item;
	}
	@Override
	public Iterator<T> iterator()  {
		Iterator<T> iterator = new Iterator<T>() {
			Nodo<T> act = null;
			public boolean hasNext() {
				if (first == last) {
					return false;
				}
				else {
					if (act == null) {
						return true;
					}else {
						return act.darSiguiente() != null;
					}
				}
			}
			public T next() {
				if (act == null) {
					act = first;
				}else {
					act = act.darSiguiente();
				}
				return act.darData();

			}

		};
		return iterator;
	}


	public Nodo<T> getFirst()
	{
		return first;
	}

	public Nodo<T> getLast()
	{
		return last;

	}
	public void copy(LinkedQueue<T> pLinked ){
		
		while(pLinked.iterator().hasNext()){
			this.enqueue(pLinked.dequeue());
			
			
		}
	}

}
