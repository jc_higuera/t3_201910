package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Stack<T> implements IStack<T> 
{

	private int cont;
	private Nodo<T> topStack;

	public Stack() {
		topStack = null;
		cont = 0;
	}

	public Nodo<T> getTopStack()
	{
		return topStack;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		Iterator<T> iterator = new Iterator<T>() {
			Nodo<T> act = null;
			public boolean hasNext() {
				if (size() == 0) {
					return false;
				}
				else {
					if (act == null) {
						return true;
					}else {
						return act.darSiguiente() != null;
					}
				}
			}
			public T next() {
				if (act == null) {
					act = topStack;
				}else {
					act = act.darSiguiente();
				}
				return act.darData();

			}

		};
		return iterator;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(cont == 0) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return cont;
	}

	@Override
	public void push(T t) {
		// TODO Auto-generated method stub
		Nodo<T> oldTop = topStack;
		topStack = new Nodo<T>(null, null, null);
		topStack.cambiarData(t);
		topStack.cambiarSiguiente(oldTop);
		cont++;

	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		if (isEmpty()) {
			throw new NoSuchElementException("Stack underflow");
		}
		T t = topStack.darData();
		topStack = topStack.darSiguiente();
		cont--;
		return t;
	}

}
