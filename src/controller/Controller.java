package controller;

import java.io.IOException;
import java.util.Scanner;

import logic.MovingViolationsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {

	private MovingViolationsManagerView view;
	private MovingViolationsManager mvm;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;

	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private IStack<VOMovingViolations> movingViolationsStack;


	public Controller() {
		view = new MovingViolationsManagerView();

		//TODO, inicializar la pila y la cola
		movingViolationsQueue = null;
		movingViolationsStack = null;
		mvm = new MovingViolationsManager();
	}

	public void run() throws IOException {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				this.loadMovingViolations();
				break;

			case 2:
				IQueue<VODaylyStatistic> dailyStatistics = this.getDailyStatistics();
				view.printDailyStatistics(dailyStatistics);
				break;

			case 3:
				view.printMensage("Ingrese el número de infracciones a buscar");
				int n = sc.nextInt();

				IStack<VOMovingViolations> violations = this.nLastAccidents(n);
				view.printMovingViolations(violations);
				break;

			case 4:	
				fin=true;
				sc.close();
				break;
			}
		}
	}



	public void loadMovingViolations() {
		// TODO
		mvm.loadMovingViolations("./data/Moving_Violations_Issued_in_January_2018_ordered.csv");
		mvm.loadMovingViolations("./data/Moving_Violations_Issued_in_February_2018_ordered.csv");
	}

	public IQueue <VODaylyStatistic> getDailyStatistics () throws IOException {
		// TODO

		return mvm.getDalyStatistics();
	}

	public IStack <VOMovingViolations> nLastAccidents(int n) {
		// TODO
		return mvm.nLastAccidents(n);
	}
}
