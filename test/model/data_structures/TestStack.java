package model.data_structures;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class TestStack extends TestCase
{
	private Stack<String> stack;

	@Before
	public void setUp() throws Exception{
		stack = new Stack<>();
	}

	@Test
	public void test() 
	{
		stack.push("a");
		stack.push("b");
		stack.push("c");
		assertEquals(3, stack.size());
		assertEquals("c", stack.pop());
		assertEquals(2, stack.size());
	}
}
