package model.data_structures;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class TestQueue extends TestCase{

	private LinkedQueue<String> linkedQueue;

	@Before
	public void setUp() throws Exception{
		linkedQueue = new LinkedQueue<>();
	}

	@Test
	public void test() {
		linkedQueue.enqueue("a");
		linkedQueue.enqueue("b");
		linkedQueue.enqueue("c");
		assertEquals(3, linkedQueue.size());
		linkedQueue.dequeue();
		assertEquals(2, linkedQueue.size());
		assertEquals("b", linkedQueue.dequeue());
	}

}
